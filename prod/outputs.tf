# esc-cluster / outputs

output "ecs_cluster" {
  value     = module.ecs-cluster
  sensitive = true
}

output "kms" {
  value     = module.kms
  sensitive = true
}

output "logs" {
  value = module.logs
}

output "loadbalancers" {
  value = module.loadbalancer
}

output "security_groups" {
  value = module.security_groups
}
