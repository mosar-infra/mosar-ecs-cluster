# ws ecs cluster / datasources

data "aws_vpc" "vpc" {
  dynamic "filter" {
    for_each = local.env_filter
    content {
      name   = filter.value.name
      values = filter.value.values
    }
  }
}

data "aws_subnets" "private" {
  dynamic "filter" {
    for_each = local.private_subnet_filter
    content {
      name   = filter.value.name
      values = filter.value.values
    }
  }
}

data "aws_subnet" "private" {
  for_each = toset(data.aws_subnets.private.ids)
  id       = each.value
}

data "aws_subnets" "public" {
  dynamic "filter" {
    for_each = local.public_subnet_filter
    content {
      name   = filter.value.name
      values = filter.value.values
    }
  }
}

data "aws_subnet" "public" {
  for_each = toset(data.aws_subnets.public.ids)
  id       = each.value
}

data "aws_acm_certificate" "mosar" {
  domain = "mosar.inquisitive.nl"
}
