# ecs-cluster/variables.tf

variable "environment" {
  default = "prod"
}
variable "managed_by" {
  default = "ecs-cluster"
}
