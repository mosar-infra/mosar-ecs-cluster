# ws ecs_cluster test /locals.tf

locals {
  kms = {
    mosar_log_key = {
      description             = "key to encrypt data between the client and the container for mosar"
      deletion_window_in_days = 7
    }
    jenkins_log_key = {
      description             = "key to encrypt data between the client and the container for jenkins"
      deletion_window_in_days = 7
    }
  }
}

locals {
  clusters = {
    jenkins = {
      cluster_name                   = "jenkins-ecs-cluster-${var.environment}"
      log_key_arn                    = module.kms.kms_key["jenkins_log_key"].arn
      container_insights_enabled     = "enabled"
      logging                        = "OVERRIDE"
      cloud_watch_encryption_enabled = true
      cloud_watch_log_group_name     = "jenkins-logs-${var.environment}"
    }
    mosar = {
      cluster_name                   = "mosar-ecs-cluster-${var.environment}"
      log_key_arn                    = module.kms.kms_key["mosar_log_key"].arn
      container_insights_enabled     = "enabled"
      logging                        = "OVERRIDE"
      cloud_watch_encryption_enabled = true
      cloud_watch_log_group_name     = "mosar-logs-${var.environment}"
    }
  }
}

locals {
  dns_namespaces = {
    mosar_dns_namespace = {
      name        = "mosar.local"
      description = "mosar namespace"
      vpc_id      = data.aws_vpc.mosar_test.id
    }
    jenkins_dns_namespace = {
      name        = "jenkins.local"
      description = "jenkins namespace"
      vpc_id      = data.aws_vpc.mosar_test.id
    }
  }
}

locals {
  subnets = {
    private = [for s in data.aws_subnet.private : s.id]
    public  = [for s in data.aws_subnet.public : s.id]
  }
}
locals {
  cidr_blocks = {
    private = [for s in data.aws_subnet.private : s.cidr_block]
    public  = [for s in data.aws_subnet.public : s.cidr_block]
  }
}

locals {
  security_groups = {
    mosar_lb = {
      name        = "mosar_lb_sg"
      vpc_id      = data.aws_vpc.mosar_test.id
      description = "security group for mosar lb"
      ingress_sg  = {}
      ingress_cidr = {
        https = {
          from        = 443
          to          = 443
          protocol    = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
        }
      }
      egress = {
        frontend = {
          from        = 80
          to          = 80
          protocol    = "tcp"
          cidr_blocks = local.cidr_blocks.private
        }
        feature_flags = {
          from        = 18000
          to          = 18000
          protocol    = "tcp"
          cidr_blocks = local.cidr_blocks.private
        }
      }
    }
    jenkins_lb = {
      name        = "jenkins_lb_sg"
      vpc_id      = data.aws_vpc.mosar_test.id
      description = "security group for jenkins lb"
      ingress_sg  = {}
      ingress_cidr = {
        https = {
          from        = 443
          to          = 443
          protocol    = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
        }
      }
      egress = {
        frontend = {
          from        = 8080
          to          = 8080
          protocol    = "tcp"
          cidr_blocks = local.cidr_blocks.private
        }
      }
    }
  }
}

locals {
  loadbalancers = {
    mosar_lb = {
      lb_name                   = "mosar-lb-${var.environment}"
      public_subnet_ids         = local.subnets.public
      public_security_group_ids = [module.security_groups.security_groups["mosar_lb"].id]
      vpc_id                    = data.aws_vpc.mosar_test.id
      listener_port             = 443
      listener_protocol         = "HTTPS"
      ssl_policy                = "ELBSecurityPolicy-2016-08"
      certificate_arn           = data.aws_acm_certificate.mosar_test.arn
      lb_tg_name                = "mosar-lb-tg"
      lb_tg_target_type         = "ip"
      lb_tg_target_id           = "" # whatever
      lb_tg_port                = 80 #nginx
      lb_tg_protocol            = "HTTP"
      lb_tg_path                = "/"
      lb_tg_healthy_threshold   = 2
      lb_tg_unhealthy_threshold = 4
      lb_tg_timeout             = 10
      lb_tg_interval            = 15
      add_attachment            = false
    }
    jenkins_lb = {
      lb_name                   = "jenkins-lb-${var.environment}"
      public_subnet_ids         = local.subnets.public
      public_security_group_ids = [module.security_groups.security_groups["jenkins_lb"].id]
      vpc_id                    = data.aws_vpc.mosar_test.id
      listener_port             = 443
      listener_protocol         = "HTTPS"
      ssl_policy                = "ELBSecurityPolicy-2016-08"
      certificate_arn           = data.aws_acm_certificate.jenkins.arn
      lb_tg_name                = "jenkins-lb-tg"
      lb_tg_target_type         = "ip"
      lb_tg_target_id           = "" # whatever
      lb_tg_port                = 8080
      lb_tg_protocol            = "HTTP"
      lb_tg_path                = "/login"
      lb_tg_healthy_threshold   = 2
      lb_tg_unhealthy_threshold = 7
      lb_tg_timeout             = 10
      lb_tg_interval            = 15
      add_attachment            = false
    }
  }
}

locals {
  private_subnet_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
    },
    {
      name   = "tag:Name"
      values = ["*private*"]
  }]
}
locals {
  public_subnet_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
    },
    {
      name   = "tag:Name"
      values = ["*public*"]
  }]
}

locals {
  env_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
  }]
}
