# ecs-cluster/variables.tf

variable "environment" {
  default = "test"
}
variable "managed_by" {
  default = "ecs-cluster"
}
