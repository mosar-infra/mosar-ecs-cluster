# ws ecs-cluster/main.tf

module "ecs-cluster" {
  source         = "git::https://gitlab.com/mosar-infra/tf-module-ecs-cluster.git?ref=tags/v1.0.2"
  clusters       = local.clusters
  dns_namespaces = local.dns_namespaces
  environment    = var.environment
  managed_by     = var.managed_by
}

module "kms" {
  source      = "git::https://gitlab.com/mosar-infra/tf-module-kms.git?ref=tags/v1.0.1"
  keys        = local.kms
  environment = var.environment
  managed_by  = var.managed_by
}

module "logs" {
  source         = "git::https://gitlab.com/mosar-infra/tf-module-logs.git?ref=tags/v1.0.1"
  for_each       = local.clusters
  log_group_name = each.value.cloud_watch_log_group_name
  environment    = var.environment
}

module "security_groups" {
  source          = "git::https://gitlab.com/mosar-infra/tf-module-security-groups.git?ref=tags/v1.2.2"
  environment     = var.environment
  managed_by      = var.managed_by
  security_groups = local.security_groups
}

module "loadbalancer" {
  source                    = "git::https://gitlab.com/mosar-infra/tf-module-loadbalancer.git?ref=tags/v1.0.6"
  for_each                  = local.loadbalancers
  lb_name                   = each.value.lb_name
  public_subnet_ids         = each.value.public_subnet_ids
  public_security_group_ids = each.value.public_security_group_ids
  vpc_id                    = each.value.vpc_id
  listener_port             = each.value.listener_port
  listener_protocol         = each.value.listener_protocol
  ssl_policy                = each.value.ssl_policy
  certificate_arn           = each.value.certificate_arn
  lb_tg_name                = each.value.lb_tg_name
  lb_tg_port                = each.value.lb_tg_port
  lb_tg_protocol            = each.value.lb_tg_protocol
  lb_tg_target_type         = each.value.lb_tg_target_type
  lb_tg_target_id           = each.value.lb_tg_target_id
  lb_tg_healthy_threshold   = each.value.lb_tg_healthy_threshold
  lb_tg_unhealthy_threshold = each.value.lb_tg_unhealthy_threshold
  lb_tg_path                = each.value.lb_tg_path
  lb_tg_timeout             = each.value.lb_tg_timeout
  lb_tg_interval            = each.value.lb_tg_interval
  add_attachment            = each.value.add_attachment
  environment               = var.environment
  managed_by                = var.managed_by
}

